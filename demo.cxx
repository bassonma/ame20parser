#include <ame20parser.h>
#include <iostream>
int main(){
    MassEval2020 ame20("mass_1.mas20");

    // let's test it out
    std::cout.precision(15);
    auto anz = ame20.NZ("a");  
    auto he3nz = ame20.NZ("he3");
    auto he4nz = ame20.NZ("he4");
    
    auto pb208nz = ame20.NZ("pb208");
    std::cout << "He3 mass = " << ame20.Mass("he3") << "u, <N,Z> = <" << he3nz.first << ", " << he3nz.second << ">" << std::endl;
    std::cout << "He4 mass = " << ame20.Mass("he4") << "u, <N,Z> = <" << he4nz.first << ", " << he4nz.second << ">" << std::endl;
    std::cout << "a   mass = " << ame20.Mass("a")   << "u, <N,Z> = <" << anz.first   << ", " << anz.second   << ">" << std::endl;
    
    std::cout << "208Pb mass = " << ame20.Mass("pb208") << "u, <N,Z> = <" << pb208nz.first << ", " << pb208nz.second << ">" << std::endl;
    std::cout << "\t in MeV/c^2 : " << ame20.Mass("pb208", true) << " MeV" << std::endl;

    // let's test the qvalue calculator
    std::cout << "7Be + 208Pb -> 209Bi + d + 4He\tQ = " << ame20.Qval((std::vector<std::string>){"7Be","208Pb"}, (std::vector<std::string>) {"a","d","209Bi"}) << " MeV" << std::endl;
    std::vector<std::pair<int,int>> exit_channel_7Be208Pb__ad = {std::make_pair(2,2), std::make_pair(1,1)};
    std::cout << "(with exit channel function overloaded version using isotope names): Q = " << ame20.Qval("7Be","208Pb",(std::vector<std::string>){"a","d"}) << " MeV" << std::endl;
    std::cout << "(with exit channel function overloaded version using NZ pairs): Q = " << ame20.Qval(std::make_pair(3,4), std::make_pair(208-82, 82), exit_channel_7Be208Pb__ad) << " MeV" << std::endl;
    std::cout << "7Be + 208Pb -> 8Be + 207Pb\tQ = " <<     ame20.Qval((std::vector<std::string>){"7Be","208Pb"}, (std::vector<std::string>) {"8Be","207Pb"}) << " MeV" << std::endl;
    

    return 0;
}
