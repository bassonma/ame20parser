# A simpler way of parsing the AME2020 datafile in C++
## Marshall Basson, basson@frib.msu.edu
The ame20parser.h file is a simple C++ header-only definition of an isotope mass table, which automatically cleans and parses the AME2020 mass file (included in this repository - this is not my own work and obtained from https://www.anl.gov/phy/reference/ame-2020-mass1mas20).

All you need to do to use it is include the header in any object and declare a MassEval2020 object.  
```
#include <ame20parser.h>
// later on
int myFunc(){
	...
	MassEval2020 ame20("path/to/mass_1.mas20");
	// do stuff with ame20; only needs to be instantiated once!
	...
}
```
You can then retrieve the mass of an isotope by referencing its name or neutron and proton numbers.  
For example, if you wanted to retrieve the masses of a few isotopes from the table in a project:
```
MassEval2020 ame20("path/to/mass_1.mas20");
long double m7be   = ame20.Mass("7be"); // Can reference using a chemical symbol
long double m208pb = ame20.Mass("PB208"); // You can use all caps or put the A number after the element symbol
long double malpha = ame20.Mass("a"); // protons, deuterons, tritons, and alphas can be accessed using shorthand p, d, t, and a
long double mhe3   = ame20.Mass(std::make_pair(1,2)); // can reference N,Z to get isotope mass
```

See demo.cxx for an more detailed example, including a demo of the Qval calculator.  You can compile it by running
```
g++ demo.cxx -o demo
```
and run it like an executable.
